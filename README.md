# README #

This README contains the files needed to run the CA2 weather chatbot in Heroku 

### Items within this folder ###

* Procfile (this tells Heroku how to run the Python app (i.e. `python app.py`, similar to `node app.js` in NodeJS) 
* requirements.txt (this contains the libraries that Heroku needs to import via `pip`)
* runtime.txt (this tells Heroku which version of the Python kernel to run)

### DialogFlow agent ###
The agent can be downloaded from [here](https://drive.google.com/file/d/1pRkxp6PWmGHsgztKhlD3iR7OFDbqxGNu/view?usp=sharing) and imported into your own DialogFlow setup.