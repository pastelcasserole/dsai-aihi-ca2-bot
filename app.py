#!/usr/bin/env python

# import urllib, json, os, sys, requests, datetime as dt, re
import json, os, sys, requests, datetime as dt, re
from flask import Flask, request, make_response, jsonify

# Flask app should start in global layout
app = Flask(__name__)

###############
## routes    ##
###############

# root route
@app.route('/')
def hello():    
    # displaying the python version is only use in dev testing
    # comment out the next two lines in production
    # python_version = "\npython-version%s\n" % sys.version
    # return (python_version)
    
    return '<h1>Hello world!</h1>'

# /webhook route, note that DialogFlow only accepts the POST method
@app.route('/webhook', methods=['POST'])
def webhook():  
    print('Executing webhook...')
    return make_response(jsonify(results()))


###############
## functions ##
###############

# function: fetch current rainfall data from NEA
def rainfall(station_name):
    # make the request to get the latest rainfall data
    r = requests.get('https://api.data.gov.sg/v1/environment/rainfall').json()
    
    # get the station_id from the station name passed in from DialogFlow
    # the [0] at the end of station_id is to convert the result from array to string
    station_id = [item['id'] for item in r['metadata']['stations']
              if item['name'] == station_name][0]
    
    # get the readings of the selected station
    # the [0] at the end of reading is to convert the result from array to string
    reading = [item['value'] for item in r['items'][0]['readings']
              if item['station_id'] == station_id][0]

    return reading


# function: fetch current weather from NEA
def weather_now(town):
    # make the request to get the latest weather data
    r = requests.get('https://api.data.gov.sg/v1/environment/2-hour-weather-forecast').json()
    
    # get the forecast of the selected town
    forecast = [item['forecast'] for item in r['items'][0]['forecasts']
                if item['area'] == town][0]

    # make the request to get the PSI data
    r_psi = requests.get('https://api.data.gov.sg/v1/environment/psi').json()
    
    # get the psi at the national level,
    # which takes the worse reading of the five regions (north, south, east, west, central)
    psi = r_psi['items'][0]['readings']['psi_twenty_four_hourly']['national']

    # .lower() converts the forecast text to lowercase
    return forecast.lower(), psi


# function: fetch the 24-hour weather forecast from NEA
def weather_24hr():
    # make the request to get the 24 hour weather data
    r = requests.get('https://api.data.gov.sg/v1/environment/24-hour-weather-forecast').json()

    # get min and max temperatures
    forecast_24hr = r['items'][0]['general']['forecast']
    forecast_24_humidity_low = r['items'][0]['general']['relative_humidity']['low']
    forecast_24_humidity_high = r['items'][0]['general']['relative_humidity']['high']
    forecast_24hr_temp_low = r['items'][0]['general']['temperature']['low']
    forecast_24hr_temp_high = r['items'][0]['general']['temperature']['high']
    
    return forecast_24hr.lower(), forecast_24_humidity_low, forecast_24_humidity_high, forecast_24hr_temp_low, forecast_24hr_temp_high


# function: fetch the four-day forecast from NEA
def weather_4day():
    # make the request to get the 24 hour weather data
    r = requests.get('https://api.data.gov.sg/v1/environment/4-day-weather-forecast').json()

    # get the four-day weather forecast
    forecast_4day_date = [item['date'] for item in r['items'][0]['forecasts']]
    forecast_4day_date_format = [dt.datetime.strptime(item, "%Y-%m-%d").date().strftime('%a %d %b %Y') 
                             for item in forecast_4day_date]
    # regex substitution is used to remove the full stop in the JSON body 
    forecast_4day = [re.sub(r'\W+', ' ', item['forecast']) for item in r['items'][0]['forecasts']]

    # construct the reply 
    forecast_4day_result = [forecast_4day_date_format[item] + ': ' 
                            + forecast_4day[item] +'\n' for item in range(0,len(forecast_4day))]
    # convert the list to string format
    forecast_4day_result = ''.join(forecast_4day_result)

    # return forecast_4day_date_format, forecast_4day
    return forecast_4day_result

# function for sending the response back to dialogflow
 # this method handles the http requests for the DialogFlow webhook
def results():
    input = request.get_json(force=True)

    # catch runtime errors 
    try:
        intent = input['queryResult']['intent']['displayName']  
    
    except Exception as e:
       print('Error:',e)
    
    # check if the intent is to get rainfall
    if intent == 'int-3-check-rainfall':
        # get the station name from the request body
        station_name = input['queryResult']['parameters']['ent-station-name']
        print('The station name passed in from DialogFlow is:',station_name)
        print('Retrieved rainfall for ' + input['queryResult']['queryText'] + ' (' + station_name + ') is ' + str(rainfall(station_name)) + 'mm')
        
        # send the requested rainfall data back to DialogFlow
        return{'fulfillmentText':'The current rainfall for ' + station_name + ' is ' + str(rainfall(station_name)) + 'mm.'}
    
    # check whether the intent is to get the current weather information
    elif intent == 'int-1-whats-the-weather':
           # get the town from the request body
           town = input['queryResult']['parameters']['ent-town']
           print('The query text passed in from DialogFlow is:',input['queryResult']['queryText'])
           print('The town name passed in from DialogFlow is:',town)
           print('Retrieved forecast for ' + input['queryResult']['queryText'] + ' (' + town + ') is ' + str(weather_now(town)))

           # send the requested forecast back to DialogFlow
           return{'fulfillmentText':'The two-hour forecast for '  +  town + ' is ' + str(weather_now(town)[0]) + ', and the current national PSI is ' + str(weather_now(town)[1]) + '.  Would you like the forecast for the next 24 hours?'}

    # check whether the standalone or follow-up intent after fetching the current weather
    # information is to get the 24 hour weather forecast
    elif intent in ['int-1-whats-the-weather-yes-24hour-forecast','int-5-forecast-next24hour']:
        # get the forecast variables from the weather_24hr() function
        print('Follow-up intent: Getting the forecast for the next 24 hours')
        forecast_24hr, forecast_24_humidity_low, forecast_24_humidity_high, forecast_24hr_temp_low, forecast_24hr_temp_high = weather_24hr()

        # send the requested forecast back to DialogFlow
        return{'fulfillmentText':'The general 24-hour forecast for Singapore is ' + str(forecast_24hr) + 
        '. The temperature will range between ' + str(forecast_24hr_temp_low) + ' and ' + str(forecast_24hr_temp_high) + 
        ' degrees Celsius, while the relative humidity will range between ' + str(forecast_24_humidity_low) + '% and ' + str(forecast_24_humidity_high) + '%.'}

    # check whether the intent is to get the four-day forecast
    elif intent == 'int-2-forecast-next-four-days':
        print('Getting the forecast for the next four days')

        # send the requested forecast back to DialogFlow
        print('Length of the response to be sent back to DialogFlow:', len('The forecast for the rest of the week is:\n\n' + str(weather_4day())))
        return{'fulfillmentText':'The forecast for the rest of the week is:\n\n' + str(weather_4day())}

    else:
        # else, return the input parameters as json
        return {'fulfillmentText':'The wrong intent was referenced to the webhook. Please try again.'}  


if __name__ == '__main__':
    # get port from environment variable or choose 9099 as local default
    port = int(os.getenv('PORT', 9099))
    # run the app, listening on all IPs with the chosen port number
    # note: there is no need to define a ip address the port number variable for Heroku, unlike NodeJS
    app.run(host='0.0.0.0', port=port, debug=True)